import numpy as np

def do_something1(x):
	for i in range(10):
		x[i] = 2*x[i]
		
	return x
	
def do_something2(x2):
	for i in range(10):
		x2[i] = 2*x2[i]
		
	return x2
	
x = np.ones((10,10))
for i in range(x.shape[0]-1):
	x[i+1, :] = do_something1(np.copy(x[i, :]))
print(x)