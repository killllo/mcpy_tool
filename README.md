Bare minimum Simulation Tool for random walks inside a 1d - bislab geometry.

Initial parameters are set so that a simulation can start. If rho > 0 the particles can exchange between the intra and extracellular space.


Open the readme file locally, otherwise the geometry is displayed correctly.
Geometry:
|               :                       |
|               :                       |
|               :                       |
|               :                       |

0               L                       Lx      -  Position of the walls. Walls at 0 and Lx are impermeable. Permeable at L.
