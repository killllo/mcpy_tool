from PyQt5 import QtWidgets, uic #for gui functionality
import sys
from numba import njit, prange #for compiling c code
import numpy as np
import matplotlib.pyplot as plt
import time


class Ui(QtWidgets.QMainWindow): #load the gui and listen for click "start simulation"
    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi('MCPY_tool.ui', self)

        self.button = self.findChild(QtWidgets.QPushButton, 'pushButton_simulate')  # Find the button
        self.button.clicked.connect(
            self.pushbutton_simulate_pressed)  # Remember to pass the definition/method, not the return value!
        self.show()

    def pushbutton_simulate_pressed(self): #pushbutton function
        plt.close("all")
        # This is executed when the button is pressed
        # Get all the values from their corresponding widgets of the ui:
        self.edit_L = self.findChild(QtWidgets.QLineEdit, 'edit_L')
        self.edit_Lx = self.findChild(QtWidgets.QLineEdit, 'edit_Lx')
        self.edit_Di = self.findChild(QtWidgets.QLineEdit, 'edit_Di')
        self.edit_De = self.findChild(QtWidgets.QLineEdit, 'edit_De')
        self.edit_N_steps = self.findChild(QtWidgets.QLineEdit, 'edit_n_steps')
        self.edit_N_particles = self.findChild(QtWidgets.QLineEdit, 'edit_n_particles')
        self.edit_startpos = self.findChild(QtWidgets.QLineEdit, 'edit_startpos')
        self.rho = self.findChild(QtWidgets.QLineEdit, 'edit_eho')
        self.geometry_dropdown = self.findChild(QtWidgets.QComboBox, 'comboBox')

        self.geometry = self.geometry_dropdown.currentText()
        # check if all the Input fields are set or not, if not a value is set in order to prevent problems
        self.L = self.edit_L.text()
        if not self.L:
            self.L = 1.0
        self.L = float(self.L)

        self.Lx = self.edit_Lx.text()
        if not self.Lx:
            self.Lx = 1.0
        self.Lx = float(self.Lx)

        self.Di = self.edit_Di.text()
        if not self.Di:
            self.Di = 1.0
        self.Di = float(self.Di)

        self.De = self.edit_De.text()
        if not self.De:
            self.De = 1.0
        self.De = float(self.De)

        self.T = self.edit_T.text()
        if not self.T:
            self.T = 1.0
        self.T = float(self.T)

        self.N_steps = self.edit_N_steps.text()
        if not self.N_steps:
            self.N_steps = 1.0
        self.N_steps = float(self.N_steps)

        self.N_particles = self.edit_N_particles.text()
        if not self.N_particles:
            self.N_particles = 1.0
        self.N_particles = float(self.N_particles)

        self.startpos = self.edit_startpos.text()
        if not self.startpos:
            self.startpos = 3.0
        self.startpos = float(self.startpos)

        self.rho = self.edit_rho.text()
        if not self.rho:
            self.rho = 0.0
        self.rho = float(self.rho)

        #after all inputs are checked and at least floats, the simulation is started:
        self.run_mc()

    def run_mc(self):
        x0 = np.zeros(int(self.N_particles))
        x0_out = np.zeros(int(self.N_particles))
        n_in_out = np.zeros(int(self.N_particles))
        x_cm = np.zeros(int(self.N_particles))
        #correct units for the inputs:
        L = self.L * 1e-6 #now [m]
        Lx = self.Lx * 1e-6 #now [m]
        N_steps = self.N_steps #steps
        Di = self.Di * 1e-9 #[m^2/s]
        De = self.De * 1e-9 #[m^2/s]
        T = self.T * 1e-3 #[s]
        rho = self.rho * 1e-3 #[m/s]
        start_pos = self.startpos #start position, if 1 -> inside, 2 -> outside, 3 -> everywhere
        Ly = 10e-6 #currently unused, just for testing

        @njit(parallel=True) #simply needed to simulate the particles in parallel
        def generate_start_1d_bislab(x0, L, Lx): #function for the initial distribution of the particles inside the 1d - bislab
            # Function isf compiled to machine code when called the first time

            for i in prange(x0.shape[0]):
                start_found = 0
                while start_found == 0:
                    x_temp = np.random.rand() * Lx
                    if x_temp < L and start_pos == 1: #particles are located "inside" and "1 -> inside" was choosen
                        start_found = 1
                    elif x_temp > L and start_pos == 2: #particles are located "outside" and "2 -> outside" was choosen
                        start_found = 1
                    elif start_pos == 3: #partciles can start everywhere
                        start_found = 1
                x0[i] = x_temp
            return x0

        @njit(parallel=True)
        def generate_start_2d_bislab(x0, y0, L, Ly, Lx): #function for the initial distribution of the particles inside the 2d - bislab, same as above. But particles also have a y-component
            # Function is compiled to machine code when called the first time

            for i in prange(x0.shape[0]):
                start_found = 0
                while start_found == 0:
                    x_temp = np.random.rand() * Lx
                    x_temp = np.random.rand() * Ly
                    if x_temp < L and start_pos == 1:
                        start_found = 1
                    elif x_temp > L and start_pos == 2:
                        start_found = 1
                    elif start_pos == 3:
                        start_found = 1
                x0[i] = x_temp
                y0[i] = y_temp
            return x0, y0

        @njit(parallel=True)
        def random_walk_1d_bislab(x_in, x_cm, L, Lx, Di, De, rho,
                                  n_in_out): #actual function for the random walk simulation of the 1d - bislab
            # Function isf compiled to machine code when called the first time
            x = x_in
            dr = 0.0
            # Threshold is necessary for determination if particles are allowed to cross the membrane at location "L" of the 1d - bislab.
            if Di > 0.0:
                threshold_i = 2.0 * rho / Di
            else:
                threshold_i = 0.0
            if De > 0.0:
                threshold_e = 2.0 * rho / De
            else:
                threshold_e = 0.0

            # Calculate the maximum distance a particle can travel during one simulation step:
            dr_i = np.sqrt(Di * 2.0 * T / N_steps)
            dr_e = np.sqrt(De * 2.0 * T / N_steps)

            dr_i = dr_i * np.sqrt(3.0) * 2.0
            dr_e = dr_e * np.sqrt(3.0) * 2.0
            
            # loop over all particles:
            for i in prange(x.shape[0]):  # Numba likes loops
                x_cm[i] = 0
                #determine where the particle is located initially:
                if x[i] < L:  # inside
                    dr = dr_i
                    threshold = threshold_i
                else:
                    dr = dr_e
                    threshold = threshold_e    
                if N_steps > 0.0:
                
                    # prepare random numbers for dx, it is faster than generate the seperately for all steps:
                    random_numbers = np.random.rand(int(N_steps))

                    #actual loop over all simulation steps:
                    for ii in range(int(N_steps)):
                        #step length:
                        dx = (random_numbers[ii]- 0.5) * dr

                        # the above dx is used as long as interaction is true. This is important if dx is large enough that the particles "bounces" between walls. It is then geometrically reflected until the total distance dx was travelled.
                        interaction = 1
                        while interaction:

                            # check if the particle intersects with the permeable membrane:
                            interaction = 0
                            tt = (L - x[i]) / dx
                            if 0.0 < tt < 1.0:
                                x_int = x[i] + dx * tt
                                interaction = 1
                                ds0 = np.sqrt((x[i] - x_int) * (x[i] - x_int)) 
                                if np.random.rand() > (ds0 * threshold): #definition of probability to cross the membrane
                                    dx = (x_int - (x[i] + dx)) #particle is reflected
                                    x[i] = x_int + dx / 1000.0 #get slightly of the intersection point to start from there for further checking of intersections
                                    dx = dx - dx / 1000.0
                                else: #allowed to cross the membrane:
                                    n_in_out[i] += 1.0
                                    dx = (x[i] + dx - (x_int))
                                    x[i] = x_int + dx / 1000.0
                                    dx = dx - dx / 1000.0
                                    if x[i] < L: #if the particle is now in the intracellular space the dr of the next step need to be adjusted to use Di instead of De. also the remaining step needs to be adjusted to be faster/slower according to Di and De.
                                        dr = dr_i
                                        threshold = threshold_i
                                        if De > 0.0:
                                            dx = dx * np.sqrt(Di / De)
                                        else:
                                            dx = 0.0
                                    else: 
                                        dr = dr_e
                                        threshold = threshold_e
                                        if Di > 0.0:
                                            dx = dx * np.sqrt(De / Di)
                                        else:
                                            dx = 0.0
                            if interaction == 0: # if no interaction has taken place it is necessary to check if the particle wants to leave the outer walls, it then need to be reflected and interaction is set to true and the particle is checked again for this step.
                                if x[i] + dx < 0.0:
                                    dx = -(x[i] + dx)
                                    x[i] = 0.001 * dx
                                    dx = dx - dx / 1000.0
                                    interaction = 1
                                if (x[i] + dx) > Lx:
                                    dx = -(x[i] + dx - Lx)
                                    x[i] = Lx + dx / 1000.0
                                    dx = dx - dx / 1000.0
                                    interaction = 1
                                x[i] = x[i] + dx
                                x_cm[i] = x_cm[i] + x[i]
                    
                    x_cm[i] = x_cm[i]/float(N_steps)
            return x, x_cm, n_in_out

        start = time.time()
        x_result = np.zeros((4, int(self.N_particles)))
        x_cm_result = np.zeros((3, int(self.N_particles)))

        if self.geometry == "1d - bislab":
            x_result[0, :] = generate_start_1d_bislab(x0, L, Lx)
            for j in range(3): #used to "simulate" a temporal gradient profile. Currently not really implemented as in the original MCTool. NEEDS TO BE DONE!!!!!
                result = random_walk_1d_bislab(np.copy(x_result[j, :]), x_cm, L, Lx, Di, De, rho, n_in_out)
                x_result[j+1, :] = result[0] #here j+1 because the start value is saved in index 0
                x_cm_result[j, :] = result[1]
                n_in_out = result[2]

        else:
            n_in_out = np.zeros(int(self.N_particles))

        print("k_ex: ", np.mean(n_in_out / T, dtype=np.float64), "/s")
        end = time.time()
        print("Time consumed in working: ", end - start)
        fig, axs = plt.subplots(2)
        fig.suptitle('Comparison between start and final position')
        axs[0].scatter(x_result[0, :], np.zeros(x0.shape[0]))
        axs[1].scatter(x_result[-1, :], np.zeros(x0.shape[0]))
        plt.show()


app = QtWidgets.QApplication(sys.argv)
window = Ui()
app.exec_()
